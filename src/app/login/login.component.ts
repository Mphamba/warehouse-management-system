import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private http: HttpClient) { }

  login(form: any): void {
    // Extract email and password from the form
    const email = form.email;
    const password = form.password;

    // Define the login endpoint of your backend API
    const loginUrl = 'http://your-backend-api.com/login';

    // Prepare the request body with email and password
    const requestBody = {
      email: email,
      password: password
    };

    // Send a POST request to the login endpoint with the form data
    this.http.post(loginUrl, requestBody).subscribe(
      (response) => {
        // Handle successful login response
        console.log('Login successful:', response);
        // You may want to redirect the user to another page upon successful login
      },
      (error) => {
        // Handle login error
        console.error('Login error:', error);
        // You can display an error message to the user
      }
    );
  }
}

# Use an official Node.js 18.x image as a base image for building
FROM node:18 AS build

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install Angular CLI
RUN npm install -g @angular/cli

# Install dependencies
RUN npm install --legacy-peer-deps
# RUN npm install

# Copy the entire project to the working directory
COPY . .

# Build the Angular application
# RUN ng build --prod
RUN ng build --configuration=production


# Use the official Nginx image as a base image for serving the Angular app
FROM nginx:alpine

# Copy the built Angular app from the previous stage to the NGINX HTML directory
COPY --from=build /app/dist/warehouse-management-system /usr/share/nginx/html

# Expose port 80
EXPOSE 80

# Command to start NGINX
CMD ["nginx", "-g", "daemon off;"]
